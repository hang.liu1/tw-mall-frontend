export function postProduct(product) {
    let options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        },
        body: JSON.stringify(product)
    }

    return fetch("http://localhost:8080/api/products", options).then(response => response.json());

}