export const getProducts = () => (dispatch) => {
    fetch('http://localhost:8080/api/products')
        .then(response => response.json())
        .then(data =>
            dispatch({
                type: 'GET_PRODUCTS',
                payload: data
            }))
};

export const editProduct = (data) => (dispatch) => {

    dispatch({
        type: 'EDIT_PRODUCT',
        payload: data
    })
};


