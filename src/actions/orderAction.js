export const addProductToOrder = (data) => (dispatch) => {
    dispatch({
        type: 'ADD_PRODUCT_TO_ORDER',
        payload: data
    })
};


export const deleteProductFromOrder = (data) => (dispatch) => {
    dispatch({
        type: 'DELETE_PRODUCT_FROM_ORDER',
        payload: data
    })
};
