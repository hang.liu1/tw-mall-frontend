const initState = {
    products : []
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'ADD_PRODUCT_TO_ORDER' :
            let data = action.payload;
            let products = state.products.slice();
            let index = products.findIndex(product => product.name === data.name);
            if (index < 0){
                data.count = 1;
                products.push(data);
            }
            else {
                products[index].count = products[index].count + 1;
            }
            return {
                ...state,
                products: products

            };
        case 'DELETE_PRODUCT_FROM_ORDER':
            return {
                ...state,
                products: state.products.filter(product => product.name !== action.payload.name)
            };
        default:
            return state;
    }
};
