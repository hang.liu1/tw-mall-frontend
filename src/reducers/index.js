import {combineReducers} from "redux";
import productReducer from "./productReducer";
import orderReducer from "./orderReducer";

const reducers = combineReducers({
    product : productReducer,
    order : orderReducer
});
export default reducers;