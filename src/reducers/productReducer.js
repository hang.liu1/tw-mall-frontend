const initState = {
    products : [],
    editedProduct: {}
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'GET_PRODUCTS':
            return {
                ...state,
                products : action.payload
            };
        case 'EDIT_PRODUCT' :
            return {
                ...state,
                editedProduct : action.payload
            };
        default:
            return state
    }
};
