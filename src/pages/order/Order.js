import React, {Component} from 'react';
import {deleteProductFromOrder} from "../../actions/orderAction";
import connect from "react-redux/es/connect/connect";
import "./order.less"

class Order extends Component {

    constructor(props) {
        super(props);
    }

    deleteProduct(product){
       this.props.deleteProductFromOrder(product);

    }

    render() {
        let products = this.props.products;
        return (
            <div className="order">
                <table>
                    <tr>
                        <th>名称</th>
                        <th>单价</th>
                        <th>数量</th>
                        <th>单位</th>
                        <th>操作</th>
                    </tr>
                    {products.map((product) => (
                        <tr>
                            <td>{product.name}</td>
                            <td>{product.price}</td>
                            <td>{product.count}</td>
                            <td>{product.unit}</td>
                            <td><button onClick={() => this.deleteProduct(product)}>删除</button></td>
                        </tr>
                    ))}
                </table>
            </div>
        );
    }
}


const mapStateToProps = state => ({
    products: state.order.products
});

const mapDispatchToProps = dispatch => ({
    deleteProductFromOrder: (data) => dispatch(deleteProductFromOrder(data)),
});


export default connect(mapStateToProps, mapDispatchToProps)(Order);

