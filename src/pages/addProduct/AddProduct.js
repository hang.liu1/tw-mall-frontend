import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import "./addProduct.less"


class AddProduct extends Component {

    postProduct(){
        event.preventDefault();
        let options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            body: JSON.stringify(this.props.editedProduct)
        }

        fetch("http://localhost:8080/api/products", options)
            .then(response => {
                if (response.ok) this.props.history.push("/");
                else alert("提交失败")

            })
    }

    render() {
        console.log(this.props.editedProduct)
        return (
            <div className="addProduct">
                <form onSubmit={this.postProduct.bind(this)}>
                <h2>添加商品</h2>
                <div>
                <label>名称</label>
                <input type="text" placeholder="名称" onChange={ event => this.props.editedProduct.name = event.target.value} required="required"/>
                </div>

                <div>
                <label>价格</label>
                <input type="number" placeholder="价格" onChange={ event => this.props.editedProduct.price = event.target.value} required="required"/>
                </div>

                <div>
                <label>单位</label>
                <input type="text" placeholder="单位"onChange={ event => this.props.editedProduct.unit = event.target.value} required="required"/>
                </div>
                <div>
                <label>图片</label>
                <input type="url" placeholder="URL" onChange={ event => this.props.editedProduct.imageUrl = event.target.value} required="required"/>
                </div>
                <button type="submit">提交</button>
                </form>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    editedProduct: state.product.editedProduct
});



export default connect(mapStateToProps)(AddProduct);

