import React, {Component, Fragment} from 'react';
import connect from "react-redux/es/connect/connect";
import {bindActionCreators} from "redux";
import Product from "../components/product/Product";
import {getProducts} from "../actions/productAction";

class Mall extends Component {
    componentDidMount() {
        this.props.getProducts();
    }

    render() {
        console.log(this.props.products)
        return (
            <Fragment>
                {this.props.products.map(product => <Product data={product}/>)}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    products: state.product.products
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getProducts
}, dispatch);




export default connect(mapStateToProps, mapDispatchToProps)(Mall);