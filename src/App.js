import React, {Component} from 'react';
import './App.less';
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router";
import Header from "./components/header/Header";
import Mall from "./pages/Mall";
import Order from "./pages/order/Order";
import AddProduct from "./pages/addProduct/AddProduct";


class App extends Component{
  render() {
    return (
      <div className='App'>

        <Router>
            <Header/>
          <Switch>
              <Route path="/mall" component={Mall}/>
              <Route path="/order" component={Order}/>
              <Route path="/products/add" component={AddProduct}/>
              <Route path={"/"} component={Mall}/>
          </Switch>
        </Router>
      </div>

    );
  }
}

export default App;