import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import {addProductToOrder, deleteProductFromOrder} from "../../actions/orderAction";
import './product.less'


class Product extends Component {

    constructor(props) {
        super(props);
        this.state = {
            click : false
        }
    }

    addToOrder(){
        this.setState({"click" : true});
        this.props.addProductToOrder(this.props.data);
        this.setState({"click" : false});

    }


    render() {
        let data = this.props.data;
        return (
            <div className="product">
                <img src={data.imageUrl}/>
                <p>{data.name}</p>
                <p>单价：{data.price}/{data.unit}</p>
                <button onClick={this.addToOrder.bind(this)} disabled={this.state.click}>+</button>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    products: state.order.products
});

const mapDispatchToProps = dispatch => ({
    deleteProductFromOrder: (data) => dispatch(deleteProductFromOrder(data)),
    addProductToOrder: (data) => dispatch(addProductToOrder(data))
});


export default connect(mapStateToProps, mapDispatchToProps)(Product);



