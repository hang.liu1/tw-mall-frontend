import React, {Component} from 'react';
import './header.less'
import {NavLink} from "react-router-dom";


class Header extends Component {
    render() {
        return (
            <div className={"header"}>
                <NavLink to={"/mall"}>商城</NavLink>
                <NavLink to={"/order"}>订单</NavLink>
                <NavLink to={"/products/add"}>添加商品</NavLink>
            </div>
        );
    }
}

export default Header;